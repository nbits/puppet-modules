# Manage a Docker registry
class role::registry {
  include profile::registry
}
