# A Nextcloud system
class role::nextcloud (
  String $domain = lookup('profile::nextcloud::domain', String, 'first', undef),
  String $www_root = "/var/www/${domain}",
) {
  include profile::nextcloud
  include profile::firewall::web

  class { 'redis':
    bind => '127.0.0.1',
    port => 0,
    unixsocketperm => '0770',
  }

  user { 'www-data':
    ensure =>  present,
    groups =>  ['redis'],
  }

  php::extension { 'redis': }

  $password = lookup('redis::requirepass', String, 'first', undef)

  $file_content = "<?php
\$CONFIG = array (
  'filelocking.enabled' => true,
  'memcache.locking' => '\OC\Memcache\Redis',
  'redis' => [
    'host'     => '/var/run/redis/redis.sock',
    'port'     => 0,
    'dbindex'  => 0,
    'password' => '${password}',
    'timeout'  => 1.5,
  ],
);"

  file { "${www_root}/config/memory-cache.distributed.config.php":
    ensure  => 'file',
    mode    => '0640',
    owner   => 'www-data',
    group   => 'www-data',
    content => $file_content,
    require => Class['profile::nextcloud'],
  }
}
