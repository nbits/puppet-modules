# A physical server
class role::server (
  String $physical_volume = '/dev/mapper/default',
  String $volume_group    = 'default',
) {
  include profile::firewall::server
  include profile::mosh
  include profile::dropbear_initramfs

  class { 'profile::lvm':
    physical_volume => $physical_volume,
    volume_group    => $volume_group,
  }

  class { 'profile::libvirt':
    volume_group    => $volume_group,
  }
}
