# A monitoring agent
class profile::monitoring::agent (
  Icinga2::CustomAttributes $vars = {},
  Optional[Stdlib::Ip::Address] $ssh_remote_address = $facts['networking']['ip'],
  String $ticket_salt = lookup('profile::monitoring::server::ticket_salt', String, 'first', 'changeme!'),
  Boolean $collectd = lookup('profile::monitoring::collectd', Boolean, 'first', false),
) {

  include profile::monitoring::agent::firewall

  if $collectd {
    include profile::monitoring::collectd
  }

  class { '::icinga2':
    features     => ['mainlog'],
    constants    => {
      'NodeName'   => $trusted['certname'],
      'ZoneName'   => $trusted['certname'],
      'TicketSalt' => $ticket_salt,
    },
    manage_repos => false,
  }

  include profile::monitoring::checkcommands

  Profile::Monitoring::Agent::Api <<|  tag == "environment:${::environment}" |>>

  icinga2::object::zone { 'global-templates':
    global => true,
  }

  $address = lookup('profile::tinc::ip', Optional[String], 'first', undef)

  if $address {
    $defaults = { 'ssh_remote_address' => $ssh_remote_address, }
    $_vars = deep_merge($defaults, $vars)
    @@icinga2::object::host{ $trusted['certname']:
      address => $address,
      vars    => $_vars,
      import  => [ 'generic-host' ],
      target  => '/etc/icinga2/conf.d/hosts.conf',
      tag     => [ "environment:${::environment}" ],
    }
  } else {
    notify { 'missing-ip-for-icinga-host':
      message => 'Set profile::tinc::ip for this node in order to add it as an Icinga2 host',
    }
  }

  @@icinga2::object::endpoint{ $trusted['certname']:
    host => $trusted['certname'],
    tag  => [ "environment:${::environment}" ],
  }

  @@icinga2::object::zone { $trusted['certname']:
    endpoints => [ $trusted['certname'] ],
    parent    => 'icinga2',
    tag       => [ "environment:${::environment}" ],
  }

}
