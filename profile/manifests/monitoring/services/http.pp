# The monitoring of one HTTP website
define profile::monitoring::services::http (
  String $http_vhost = $name,
  String $http_address = $http_vhost,
  String $http_uri = '/',
  Integer $https_port = 443,
  Hash $config = {},
  Optional[String] $endpoint = undef,
  String $http_service_check_interval = '30m',
  String $http_service_retry_interval = '5m',
  String $command_endpoint = lookup('profile::monitoring::services::http_endpoint', String, 'first', $trusted['certname']),
) {

  $ip_vars = {
    'http_ipv4' => true,
    'http_ipv6' => false,
  }

  $https_vars = {
    'http_port'                        => $https_port,
    'http_ssl'                         => true,
    'http_ssl_force_tlsv1_2_or_higher' => true,
  }

  $common_vars = {
    'http_vhost'       => $http_vhost,
    'http_address'     => $http_address,
    'http_uri'         => $http_uri,
  }

  $conn_vars = deep_merge($ip_vars, $https_vars)
  $default_vars = deep_merge($conn_vars, $common_vars)
  $vars = deep_merge($default_vars, $config['vars'])

  $custom_groups = $config['groups'] ? {
    undef   => [],
    default => $config['groups'],
  }

  $http_redirect_vars = deep_merge($common_vars, {
    http_expect       => 'HTTP/1.1 301 Moved Permanently',
    http_headerstring => "Location: https://${http_vhost}/",
  })

  icinga2::object::service{ "HTTP Redirect: ${name}":
    check_command      => 'http',
    max_check_attempts => 23,
    host_name          => $config['host_name'],
    zone               => 'icinga2',
    command_endpoint   => $command_endpoint,
    vars               => $http_redirect_vars,
    groups             => [ 'websites' ] + $custom_groups,
    import             => [ 'generic-service' ],
    target             => '/etc/icinga2/conf.d/services.conf',
    check_interval     => $http_service_check_interval,
    retry_interval     => $http_service_retry_interval,

  }

  icinga2::object::service{ "HTTPS: ${name}":
    check_command      => 'http',
    max_check_attempts => 23,
    host_name          => $config['host_name'],
    zone               => 'icinga2',
    command_endpoint   => $command_endpoint,
    vars               => $vars,
    groups             => [ 'websites' ] + $custom_groups,
    import             => [ 'generic-service' ],
    target             => '/etc/icinga2/conf.d/services.conf',
    check_interval     => $http_service_check_interval,
    retry_interval     => $http_service_retry_interval,
  }

  icinga2::object::service{ "TLS: ${name}":
    check_command    => 'ssl_cert',
    host_name        => $config['host_name'],
    zone             => 'icinga2',
    command_endpoint => $command_endpoint,
    vars             => {
      ssl_cert_address  => $http_vhost,
      ssl_cert_cn       => $http_vhost,
      ssl_cert_port     => $vars['http_port'],
      ssl_cert_warn     => 14,
      ssl_cert_critical => 7,
    },
    groups           => [ 'certs' ] + $custom_groups,
    import           => [ 'generic-service' ],
    target           => '/etc/icinga2/conf.d/services.conf',
    check_interval   => $http_service_check_interval,
    retry_interval   => $http_service_retry_interval,
  }
}
