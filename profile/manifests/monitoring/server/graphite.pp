# Configure an instance of Graphite, send Icinga2 data there, and show graphs
# in Icingaweb2 using graphite-web
class profile::monitoring::server::graphite (
  Stdlib::IP::Address::V4::Nosubnet $ip = lookup('profile::tinc::ip', String, 'first', $facts['networking']['ip']),
  Boolean $collectd = lookup('profile::monitoring::collectd', Boolean, 'first', false),
) {

  unless $facts['os']['distro']['codename'] =~ /bookworm/ {
    fail('This class only works for Debian Bookworm.')
  }

  ### Carbon-cache

  ensure_packages([ 'graphite-carbon', 'graphite-web', 'gunicorn3', 'sqlite3' ])

  service { 'carbon-cache':
    ensure => running,
  }

  ### Icinga2 and Icingaweb2 support for Graphite

  # XXX: This is needed so this class can be compiled by itself.
  if ! defined(Class['::icinga2']) {
    include icinga2
  }

  class { 'icinga2::feature::graphite':
    host                   => '127.0.0.1',
    port                   => 2003,
    enable_send_thresholds => true,
    enable_send_metadata   => true,
  }

  class { 'icingaweb2::module::graphite':
    install_method => 'package',
    url            => 'http://127.0.0.1:8080',
  }

  # Let's add a nices Memory graph
  file { '/usr/share/icingaweb2/modules/graphite/templates/memory.ini':
    ensure  => file,
    source  => 'puppet:///modules/profile/monitoring/memory.ini',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Class['icingaweb2::module::graphite'],
  }

  ### Graphite Web

  exec { 'Initialize Graphite DB':
    command => '/usr/bin/graphite-manage migrate',
    user    => '_graphite',
    unless  => '/usr/bin/test -n "$( /usr/bin/sqlite3 /var/lib/graphite/graphite.db .tables )"',
    require => [
      Package['sqlite3'],
      Class['icingaweb2::module::graphite'],
    ],
  }

  systemd::unit_file { 'graphite-web.socket':
    source => 'puppet:///modules/profile/monitoring/graphite-web.socket',
  }

  systemd::unit_file { 'graphite-web.service':
    source => 'puppet:///modules/profile/monitoring/graphite-web.service',
    enable => true,
    active => true,
  }

  include profile::nginx

  nginx::resource::server { 'graphite-web':
    server_name          => [ '127.0.0.1' ],
    listen_port          => 8081,
    use_default_location => false,
  }

  nginx::resource::location { 'graphite-web-gunicorn':
    server   => 'graphite-web',
    location => '/',
    proxy    => 'http://127.0.0.1:8080/'
  }

  file { '/etc/default/graphite-carbon':
    ensure  => file,
    source  => 'puppet:///modules/profile/monitoring/graphite-carbon/etc-default',
    require => Package['graphite-carbon'],
  }

  nginx::resource::location { 'graphite-web-static':
    server   => 'graphite-web',
    location => '/static/',
    www_root => '/usr/share/graphite-web',
  }

  file { '/usr/lib/python3/dist-packages/graphite/local_settings.py':
    ensure  => link,
    target  => '/etc/graphite/local_settings.py',
    require => Package['graphite-web'],
    notify  => Service['graphite-web'],
  }

  service { 'graphite-web':
    ensure  => running,
    require => [
      File['/usr/lib/python3/dist-packages/graphite/local_settings.py'],
      Service['carbon-cache'],
    ],
  }

  ### Collectd

  if $collectd {
    @@file { '/etc/collectd/collectd.conf.d/graphite.conf':
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0750',
      content => epp('profile/monitoring/collectd/graphite.conf.epp', {
        name => $trusted['certname'],
        host => $ip,
        port => 2003,
      }),
      require => Package['collectd'],
      tag     => [ 'collectd_graphite', "environment::${::environment}" ],
    }

    include profile::monitoring::collectd
  }

}
