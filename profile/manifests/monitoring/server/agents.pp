# Monitored agents
class profile::monitoring::server::agents {
  Icinga2::Object::Host <<| tag == "environment:${::environment}" |>>
  Icinga2::Object::Endpoint <<| tag == "environment:${::environment}" |>>
  Icinga2::Object::Zone <<| tag == "environment:${::environment}" |>>
}
