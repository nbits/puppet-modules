# A monitoring agent's API config
define profile::monitoring::agent::api (
  String $server_ip = '127.0.0.1',
  String $server_domain = $trusted['certname'],
  String $ticket_salt = lookup('profile::monitoring::server::ticket_salt', String, 'first', 'changeme!'),
  Stdlib::IP::Address::V4::Nosubnet $ip = lookup('profile::tinc::ip', String, 'first', $facts['networking']['ip']),
) {

  class { '::icinga2::feature::api':
    accept_config   => true,
    accept_commands => true,
    bind_host       => $ip,
    ca_host         => $server_ip,
    ticket_salt     => $ticket_salt,
    endpoints       => {
      'NodeName'     => {},
      $server_domain => {
        'host' => $server_ip,
      },
    },
    zones           => {
      'ZoneName' => {
        'endpoints' => [ 'NodeName' ],
        'parent'    => 'icinga2',
      },
      'icinga2'  => {
        'endpoints' => [ $server_domain ],
      },
    },
  }

}
