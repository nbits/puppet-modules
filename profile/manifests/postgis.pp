# Install PostGIS and PostgreSQL
class profile::postgis (
  Hash $databases = {},
  Stdlib::IP::Address::V4::Nosubnet $listen_address = '127.0.0.1',
  Boolean $tls = false,
  String $domain = $::fqdn,
  Hash $config_entries = {},
) {

  # Support for TLS encryption

  if $tls {

      $ssl_cert_file = '/var/lib/postgresql/tls/fullchain.pem'
      $ssl_key_file = '/var/lib/postgresql/tls/privkey.pem'

      class { 'profile::postgis::tls':
        domain         => $domain,
        listen_address => $listen_address,
        ssl_cert_file  => $ssl_cert_file,
        ssl_key_file   => $ssl_key_file,
      } ~> Service['postgresql']

      $ssl_config_entries = {
        ssl           => 'on',
        ssl_cert_file => $ssl_cert_file,
        ssl_key_file  => $ssl_key_file,
      }

      $server_require = [
        File[$ssl_key_file],
        File[$ssl_cert_file],
      ]

  } else {

    $ssl_config_entries = {}
    $server_require = undef

  }

  # PostGIS dependencies

  class { 'postgresql::server':
    listen_addresses     => $listen_address,
    pg_hba_conf_defaults => false,
    config_entries       => merge($ssl_config_entries, $config_entries),
    require              => $server_require,
  }

  firewall { '100 accept connections on PostgreSQL port':
    dport       => [ 5432 ],
    proto       => 'tcp',
    action      => 'accept',
    destination => "${listen_address}/32",
  }

  ensure_packages([
    'postgresql-13-postgis-3',
    'postgresql-13-postgis-3-scripts',
    'postgis',
  ])

  # Minimal default host-based authentication

  postgresql::server::pg_hba_rule { 'Allow user postgres to access all databases (via ident)':
    description => 'Allow user postgres to access all databases (via ident)',
    type        => 'local',
    database    => 'all',
    user        => 'postgres',
    auth_method => 'ident',
  }

  postgresql::server::pg_hba_rule { 'Allow user postgres to access all databases (via host)':
    description => 'Allow user postgres to access all databases (via host)',
    type        => 'host',
    database    => 'all',
    user        => 'postgres',
    address     => '127.0.0.1/32',
    auth_method => 'md5',
  }

  # Databases configuration

  $databases.each | String $database, Hash $config | {
    profile::postgis::database { $database:
      config         => $config,
      listen_address => $listen_address,
    }
  }

}
