# Basic installation of Podman, a container manager
class profile::podman {

  sysctl { 'kernel.unprivileged_userns_clone':
    value => 1,
  }

  ensure_packages([ 'uidmap', 'slirp4netns', 'fuse-overlayfs', 'catatonit', 'containers-storage' ])

  class { 'podman':
    manage_subuid            => true,
    podman_docker_pkg_ensure => 'absent',  # podman-docker is currently only available in Debian experimental repo
    require                  => [
      Sysctl['kernel.unprivileged_userns_clone'],
      Package[
        'uidmap',
        'slirp4netns',
        'fuse-overlayfs',
        'catatonit',
        'containers-storage',
      ],
    ],
  }
}
