# Schleuder profile
class profile::schleuder {

  ensure_packages([ 'schleuder', 'schleuder-cli', 'postfix-sqlite' ])

  postfix::config { 'schleuder_destination_recipient_limit':
    value => '1',
  }

  file { '/etc/postfix/schleuder_domain_sqlite.cf':
    ensure => file,
    source => 'puppet:///modules/profile/postfix/schleuder_domain_sqlite.cf',
    owner  => root,
    group  => root,
    mode   => '0644',
  }

}
