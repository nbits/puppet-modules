# A Nextcloud installation
class profile::nextcloud (
  String  $db_pass                 = 'password',
  String  $domain                  = 'example.org',
  Boolean $ssl                     = false,
  Integer $php_max_execution_time  = 90,
  String  $php_memory_limit        = '512M',
  String  $php_post_max_size       = '2M',
  String  $php_upload_max_filesize = '2M',
  Boolean $auto_logout             = true,
  Boolean $session_keepalive       = true,
  Integer $session_lifetime        = 10800,
  Stdlib::Absolutepath $www_root   = "/var/www/${domain}",
  Optional[Stdlib::Absolutepath] $datadirectory = undef,
  String $mysql_db_charset = 'utf8',
  String $mysql_db_collate = 'utf8_general_ci',
) {

  #
  # Webserver
  #

  class { 'profile::nginx':
    extra_mime_types => {'application/javascript' => 'js mjs'},
  }

  include profile::mysql::server

  class { '::php':
    settings     => {
      'PHP/max_execution_time'                  => $php_max_execution_time,
      'PHP/memory_limit'                        => $php_memory_limit,
      'PHP/post_max_size'                       => $php_post_max_size,
      'PHP/upload_max_filesize'                 => $php_upload_max_filesize,
      # Nextcloud's session lifetime is limited by PHP, so we make sure to set
      # a sane parameter here. For details, see:
      # https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/config_sample_php_parameters.html#user-experience
      'Session/session.gc_maxlifetime'          => 2 * $session_lifetime,
      'opcache/opcache.interned_strings_buffer' => 32,
      'opcache/opcache.memory_consumption'      => 256,
    },
    cli_settings => {
      'PHP/apc.enable_cli' => '1',
    },
  }

  #
  # Database
  #

  profile::mysql::db { 'nextcloud':
    user => 'nextcloud',
    host => 'localhost',
    pass => $db_pass,
    charset => $mysql_db_charset,
    collate => $mysql_db_collate,
  }

  #
  # PHP
  #

  $php_socket = '/var/run/php/nextcloud'

  php::fpm::pool { 'nextcloud':
    listen       => $php_socket,
    listen_owner => www-data,
    listen_group => www-data,
    env_value    => {
      'HOSTNAME' => $::hostname,
      'PATH'     => '/usr/bin:/bin',
      'TMP'      => '/tmp',
      'TMPDIR'   => '/tmp',
      'TEMP'     => '/tmp',
    }
  }

  ensure_packages([
    'php-bcmath',
    'php-bz2',
    'php-curl',
    'php-gd',
    'php-gmp',
    'php-imagick',
    'php-intl',
    'php-mbstring',
    'php-mysql',
    'php-sqlite3',
    'php-zip',
  ], { ensure => latest })

  #
  # Nginx
  #

  file { $www_root:
    ensure => directory,
    owner  => www-data,
    group  => www-data,
    mode   => '0750',
  }

  $server_cfg_prepend = {
      # Remove X-Powered-By, which is an information leak
      'fastcgi_hide_header'  => 'X-Powered-By',
      # set max upload size
      'client_max_body_size' => '512M',
      'fastcgi_buffers'      => '64 4K',
      # Enable gzip but do not remove ETag headers
      'gzip'                 => 'on',
      'gzip_vary'            => 'on',
      'gzip_comp_level'      => '4',
      'gzip_min_length'      => '256',
      'gzip_proxied'         => 'expired no-cache no-store private no_last_modified no_etag auth',
      'gzip_types'           => 'application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/wasm application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy', # lint:ignore:140chars
      root                   => $www_root,
  }

  $_www_root = $ssl ? { false => $www_root, default => undef }
  $_server_cfg_ssl_prepend = $ssl ? { false => undef, default => $server_cfg_prepend }
  $_server_cfg_prepend = $ssl ? { false => $server_cfg_prepend, default => undef }

  nginx::resource::map { 'asset_immutable':
    default  => '", immutable"',
    string   => '$arg_v',
    mappings => {'""' => '""'},
  }

  profile::website { $domain:
    www_root               => $_www_root,
    add_header             => {
      'Strict-Transport-Security'         => {'max-age=15768000; includeSubDomains;' => 'always'},  # TODO: add '; preload'
      'Referrer-Policy'                   => { 'no-referrer' => always },
      'X-Content-Type-Options'            => { 'nosniff' => always },
      'X-Download-Options'                => { 'noopen' => always },
      'X-Frame-Options'                   => { 'SAMEORIGIN' => always },
      'X-Permitted-Cross-Domain-Policies' => { 'none' => always },
      'X-Robots-Tag'                      => { 'noindex, nofollow' =>  always },
      'X-XSS-Protection'                  => { '1; mode=block' => always },
    },
    server_cfg_ssl_prepend => $_server_cfg_ssl_prepend,
    server_cfg_prepend     => $_server_cfg_prepend,
    ssl                    => $ssl,
    rewrite_rules          => [ '^ /index.php' ],
    index_files            => [],
  }

  $proto = $ssl ? { true => 'https', default => '$http_x_forwarded_proto' }
  $host = $ssl ? { true => $domain, default => '$host' }

  nginx::resource::location { '= /robots.txt':
    server              => $domain,
    ssl                 => $ssl,
    index_files         => [],
    location_allow      => ['all'],
    location_cfg_append =>  {
      'log_not_found' => 'off',
      'access_log'    =>  'off',
    },
  }

  nginx::resource::location { '~ ^/\.well-known/(carddav|caldav)':
    server              => $domain,
    location_cfg_append => { 'return' => "301 ${proto}://${host}/remote.php/dav/", },
    index_files         => [],
    ssl_only            => $ssl,
    priority            => 496,
  }

  nginx::resource::location { '~ ^/\.well-known':
    server              => $domain,
    location_cfg_append => { 'return' => "301 ${proto}://${host}/index.php\$request_uri", },
    index_files         => [],
    ssl_only            => $ssl,
    priority            => 497,
  }

  nginx::resource::location { '~ ^\/(?:build|tests|config|lib|3rdparty|templates|data)\/':
    server        => $domain,
    location_deny => [ 'all' ],
    index_files   => [],
    ssl_only      => $ssl,
    priority      => 498,
  }

  nginx::resource::location { '~ ^\/(?:\.|autotest|occ|issue|indie|db_|console)':
    server        => $domain,
    location_deny => [ 'all' ],
    index_files   => [],
    ssl_only      => $ssl,
    priority      => 499,
  }

  nginx::resource::location { 'php':
    server               => $domain,
    location_cfg_prepend => {
      'root'                    => $www_root,
      'fastcgi_split_path_info' => '^(.+?\.php)(\/.*|)$',
      'set'                     => '$path_info $fastcgi_path_info',
      'try_files'               => '$fastcgi_script_name =404',
    },
    location             => '~ ^\/(?:index|remote|public|cron|core\/ajax\/update|status|ocs\/v[12]|updater\/.+|ocs-provider\/.+|.+\/richdocumentscode\/proxy)\.php(?:$|\/)', # lint:ignore:140chars
    fastcgi              => "unix:${php_socket}",
    fastcgi_param        => {
      'SCRIPT_FILENAME'         => '$document_root$fastcgi_script_name',
      'PATH_INFO'               => '$path_info',
      'HTTPS'                   => 'on',
      'modHeadersAvailable'     => 'true', # lint:ignore:quoted_booleans
      'front_controller_active' => 'true', # lint:ignore:quoted_booleans
    },
    location_cfg_append  => {
      'fastcgi_intercept_errors'  => 'on',
      'fastcgi_request_buffering' => 'off',
    },
    ssl_only             => $ssl,
    priority             => 500,
  }

  nginx::resource::location { '~ ^\/(?:updater|ocs-provider)(?:$|\/)':
    server      => $domain,
    try_files   => [ '$uri/ =404' ],
    index_files => [ 'index.php' ],
    ssl_only    => $ssl,
    priority    => 501,
  }

  nginx::resource::location { '~ \.(?:css|js|mjs|svg|gif|ico|jpg|png|webp|wasm|tflite|map|ogg|flac)$':
    server              => $domain,
    try_files           => [ '$uri /index.php$request_uri' ],
    add_header          => {
      'Cache-Control'                     => 'public, max-age=15778463$asset_immutable',
      'Referrer-Policy'                   => {'no-referrer' => 'always'},
      'X-Content-Type-Options'            => {'nosniff' => 'always'},
      'X-Download-Options'                => {'noopen' => 'always'},
      'X-Frame-Options'                   => {'SAMEORIGIN' => 'always'},
      'X-Permitted-Cross-Domain-Policies' => {'none' => 'always'},
      'X-Robots-Tag'                      => {'none' => 'always'},
      'X-XSS-Protection'                  => {'1; mode=block' => 'always'},
    },
    # Optional: Don't log access to assets
    location_cfg_append => {'access_log' => 'off'},
    index_files         => [],
    ssl_only            => $ssl,
    priority            => 502,
  }

  nginx::resource::location { '~ \.(otf|woff2?)$':
    server              => $domain,
    try_files           => [ '$uri /index.php$request_uri' ],
    expires             => '7d',
    # Optional: Don't log access to assets
    location_cfg_append => {'access_log' => 'off'},
    index_files         => [],
    ssl_only            => $ssl,
    priority            => 502,
  }

  nginx::resource::location { '~ \.(?:png|html|ttf|ico|jpg|jpeg|bcmap|mp4|webm)$':
    server              => $domain,
    try_files           => [ '$uri /index.php$request_uri' ],
    # Optional: Don't log access to other assets
    location_cfg_append => {'access_log' => 'off'},
    index_files         => [],
    ssl_only            => $ssl,
    priority            => 503,
  }

  #
  # Background jobs
  #

  cron { "nextcloud-background-job-${domain}":
    command => "php --define apc.enable_cli=1 ${www_root}/cron.php",
    user    => www-data,
    minute  => '*/5',
  }

  #
  # Config directory
  #

  file { "${www_root}/config":
    ensure => directory,
    owner  => www-data,
    group  => www-data,
    mode   => '0755',
  }

  #
  # CLI config
  #

  file { "${www_root}/config/cli.config.php":
    ensure  => file,
    owner   => www-data,
    group   => www-data,
    mode    => '0644',
    content => epp('profile/nextcloud/cli.config.php.epp', {
      url => "https://${domain}",
    }),
    require => File["${www_root}/config"],
  }

  #
  # Trusted domains
  #

  file { "${www_root}/config/trusted_domains.config.php":
    ensure  => file,
    owner   => www-data,
    group   => www-data,
    mode    => '0644',
    content => epp('profile/nextcloud/trusted_domains.config.php.epp', {
      trusted_domains => [ $domain ],
    }),
    require => File["${www_root}/config"],
  }

  #
  # Memory Cache
  #

  ensure_packages(['php-apcu'])

  file { "${www_root}/config/memory-cache.config.php":
    ensure  => file,
    owner   => www-data,
    group   => www-data,
    mode    => '0644',
    source  => 'puppet:///modules/profile/nextcloud/memory-cache.config.php',
    require => [
      File["${www_root}/config"],
      Package['php-apcu'],
    ]
  }

  #
  # Localization
  #

  file { "${www_root}/config/l10n.config.php":
    ensure  => file,
    owner   => www-data,
    group   => www-data,
    mode    => '0644',
    source  => 'puppet:///modules/profile/nextcloud/l10n.config.php',
    require => File["${www_root}/config"],
  }

  #
  # E-mail configuration
  #

  file { "${www_root}/config/mail.config.php":
    ensure  => file,
    owner   => www-data,
    group   => www-data,
    mode    => '0644',
    content => epp('profile/nextcloud/mail.config.php.epp', {
      mail_from_address => 'noreply',
      mail_domain       => $domain,
    }),
    require => File["${www_root}/config"],
  }

  #
  # Session configuration
  #

  file { "${www_root}/config/session.config.php":
    ensure  => file,
    owner   => www-data,
    group   => www-data,
    mode    => '0644',
    content => epp('profile/nextcloud/session.config.php.epp', {
      auto_logout       => $auto_logout,
      session_keepalive => $session_keepalive,
      session_lifetime  => $session_lifetime,
    }),
    require => File["${www_root}/config"],
  }

  #
  # Data directory
  #

  $ensure_datadirectory = $datadirectory ? { Stdlib::Absolutepath => 'file', default => 'absent' }

  file { "${www_root}/config/datadirectory.config.php":
    ensure  => $ensure_datadirectory,
    owner   => www-data,
    group   => www-data,
    mode    => '0644',
    content => epp('profile/nextcloud/datadirectory.config.php.epp', {
      datadirectory => $datadirectory,
    }),
    require => File["${www_root}/config"],
  }

  #
  # Periodic apps update
  #

  file { "${www_root}/config/maintenance_window_start.config.php":
    ensure  => file,
    owner   => www-data,
    group   => www-data,
    mode    => '0644',
    source  => 'puppet:///modules/profile/nextcloud/maintenance_window_start.config.php',
    require => File["${www_root}/config"],
  }

  cron { 'occ app:update --all':
    command => "php ${www_root}/occ app:update --all",
    user    => www-data,
    hour    => 5,
    minute  => 0,
    weekday => 6,
  }

  #
  # Monitoring
  #

  class { 'profile::nextcloud::monitoring':
    domain => $domain,
  }

  #
  # SVG support
  #

  ensure_packages([ 'libmagickcore-6.q16-6-extra' ])

  #
  # Backups
  #

  include profile::backups::source

  profile::backups::borg { 'nextcloud':
    backup_path => "${www_root} ${profile::mysql::server::backup_dir} ${String($datadirectory)}",
  }

}
