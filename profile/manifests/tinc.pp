# Configure a VPN using tinc in switch mode
class profile::tinc (
  String $netname = 'tun0',
  Array[String] $connect_to = [],
  Optional[String] $ip = undef,
  Enum['client', 'server'] $type = 'client',
  String $tld = $netname,
) {

  $config_dir = "/etc/tinc/${netname}"

  ensure_packages(['tinc'])

  file { $config_dir:
    ensure  => directory,
    mode    => '0755',
    owner   => root,
    group   => root,
    require => Package['tinc'],
  }

  $tinc_name = regsubst($trusted['certname'], '[^[a-zA-Z0-9]]', '_', 'G')

  file { "${config_dir}/hosts":
    ensure  => directory,
    mode    => '0755',
    owner   => root,
    group   => root,
    recurse => true,
    purge   => true,
    require => File[$config_dir],
  } -> Class['profile::tinc::hosts']

  class { 'profile::tinc::hosts':
    tinc_name  => $tinc_name,
    netname    => $netname,
    config_dir => $config_dir,
    ip         => $ip,
  } ~> Service["tinc@${netname}"]

  file { "${config_dir}/tinc.conf":
    ensure  => file,
    mode    => '0644',
    owner   => root,
    group   => root,
    content => epp('profile/tinc/tinc.conf.epp', {
      name       => $tinc_name,
      connect_to => $connect_to,
      interface  => $netname,
    }),
    require => File[$config_dir],
  } ~> Service["tinc@${netname}"]

  file { "${config_dir}/tinc-up":
    ensure  => file,
    mode    => '0755',
    owner   => root,
    group   => root,
    content => epp('profile/tinc/tinc-up.epp', { ip => $ip }),
    require => File[$config_dir],
  } ~> Service["tinc@${netname}"]

  service { "tinc@${netname}":
    ensure => running,
    enable => true,
  }

  if $ip {
    @@host { "${trusted['certname']}.${tld}":
      ip  => $ip,
      tag => [ 'tinc_host_def', $netname ],
    }
  }

  Host <<| tag == 'tinc_host_def' and tag == $netname |>>

  class { 'profile::tinc::firewall':
    netname => $netname,
    type    => $type,
  }

}
