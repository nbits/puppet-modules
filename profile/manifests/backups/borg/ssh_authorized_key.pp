# An SSH authorized key to be distributed to a backup target
define profile::backups::borg::ssh_authorized_key (
  String                  $type,
  String                  $key,
  Stdlib::Absolutepath    $repo,
  Stdlib::IP::Address::V4 $source_ip,
) {

  $options = [
    'restrict',
    "from=\"${source_ip}\"",
    "command=\"borg serve --append-only --restrict-to-path ${repo}\"",
    'no-pty',
    'no-agent-forwarding',
    'no-port-forwarding',
    'no-X11-forwarding',
    'no-user-rc',
  ]

  ssh_authorized_key { $name:
    user    => 'backup',
    key     => $key,
    type    => $type,
    options => $options,
  }

}
