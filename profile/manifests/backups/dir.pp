# A borg repostory
define profile::backups::dir {

  file { $name:
    ensure  => directory,
    owner   => 'backup',
    group   => 'backup',
    mode    => '0700',
    require => Accounts::User['backup'],
  }

}
