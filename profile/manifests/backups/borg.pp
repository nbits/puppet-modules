# Configure a borg backup originating from this node
define profile::backups::borg (
  String               $backup_path,
  String               $target        = lookup('profile::backups::target', String, 'first'),
  Stdlib::Absolutepath $prefix = lookup('profile::backups::target::prefix', Stdlib::Absolutepath, 'first'),
  String               $passphrase    = lookup('profile::backups::passphrase', String, 'first'),
) {

  $repo = "${prefix}/${trusted['certname']}/${name}"

  @@profile::backups::borg::repo { $repo:
    require => Profile::Backups::Dir["${prefix}/${trusted['certname']}"],
    tag     => $target,
  }

  $type = $facts['backup_sshkey'][0]
  $key = $facts['backup_sshkey'][1]

  @@profile::backups::borg::ssh_authorized_key { "borg backup: ${name} (${trusted['certname']})":
    type      => $type,
    key       => $key,
    repo      => $repo,
    source_ip => lookup('profile::tinc::ip', Stdlib::IP::Address::V4, 'first'),
    tag       => $target,
  }

  $port = lookup('profile::sshd::ssh_port', Integer, 'first', 22)
  $tld = lookup('profile::tinc::tld', String, 'first', '')
  $target_url = "ssh://backup@${target}.${tld}:${port}"

  borg::backup { $name:
    backup_path  => $backup_path,
    archive_name => $name,
    archive_path => "${target_url}${repo}",
    passphrase   => $passphrase,
    daily        => 7,
    weekly       => 4,
    monthly      => 12,
    log          => true,
    type         => 'borg',
    cron_time    => '0 4 * * *',
  }

  @@profile::monitoring::services::borg { "Borg: ${name} (${trusted['certname']} --> ${target})":
    source        => $trusted['certname'],
    borg_log_file => "/var/log/borg/borg-${name}.log",
    tag           => "environment:${::environment}"
  }

}
