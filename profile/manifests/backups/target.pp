# Configure a target for backups
class profile::backups::target (
  Stdlib::Absolutepath $prefix,
) {

  accounts::user { 'backup':
    shell         => '/bin/sh',
    home          => '/var/backups/remote',
    home_mode     => '0700',
    purge_sshkeys => true,
  }

  Profile::Backups::Dir <<| tag == $trusted['certname'] |>>
  Profile::Backups::Borg::Repo <<| tag == $trusted['certname'] |>>
  Profile::Backups::Borg::Ssh_authorized_key <<| tag == $trusted['certname'] |>>

  # XXX distribute SSH fingerprint to other nodes

  ensure_packages(['borgbackup'])

}
