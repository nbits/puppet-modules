# A network used by VMs
#
# Setup forwarding (with SNAT) from the network to the Internet.
#
# Note: The firewall rules assume the host set as VMs router by Libvirt.
#
define profile::libvirt::network (
  String $bridge       = 'virbr0',
  String $forward_mode = 'open',
  String $ip_address   = '192.168.122.1',
  String $ip_netmask   = '255.255.255.0',
  String $dhcp_start   = '192.168.122.2',
  String $dhcp_end     = '192.168.122.254',
  String $subnet       = '192.168.122.0/24',
  Boolean $autostart   = true,
) {

  $libvirt_net_bridge = {
    name  => $bridge,
    stp   => 'on',
    delay => 0,
  }

  libvirt::network { $name:
    bridge       => $libvirt_net_bridge,
    ips => [
      {
        address => $ip_address,
        netmask => $ip_netmask,
        dhcp    => {
          range => [
            {
              start => $dhcp_start,
              end   => $dhcp_end,
            }
          ],
        },
      }
    ],
    autostart    => $autostart,
    template     => 'generic',
    forward      => { mode => $forward_mode },
  }

  # Firewall: allow DHCP and DNS traffic in the bridge

  firewall { "100 allow DHCP from bridge ${bridge}":
    iniface => $bridge,
    sport   => [67, 68],
    dport   => [67, 68],
    proto   => 'udp',
    action  => 'accept',
  }

  firewall { "100 allow DNS inside bridge ${bridge}":
    iniface     => $bridge,
    dport       => 53,
    proto       => 'udp',
    destination => $subnet,
    action      => 'accept',
  }

  # Firewall: Forward connection to the Internet

  firewall { "100 forward from bridge ${bridge}":
    chain    => 'FORWARD',
    proto    => 'all',
    iniface  => $bridge,
    outiface => $facts["networking"]["primary"],
    action   => 'accept',
  }

  firewall { "100 allow related/established to subnet ${subnet}":
    chain       => 'FORWARD',
    destination => $subnet,
    proto       => 'all',
    ctstate     => ['RELATED', 'ESTABLISHED'],
    action      => 'accept',
  }

  firewall { "100 SNAT from subnet ${subnet}":
    table    => 'nat',
    proto    => 'all',
    chain    => 'POSTROUTING',
    source   => $subnet,
    outiface => $facts["networking"]["primary"],
    jump     => 'MASQUERADE'
  }

}
