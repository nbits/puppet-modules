# A reverse_proxy configuration
define profile::reverse_proxy (
  String $proxy,
  Boolean $ssl = true,
  Integer $ssl_port = 443,
  Stdlib::Absolutepath $acme_webroot = $profile::letsencrypt::acme_webroot,
  Array[String] $proxy_set_header = [],
) {

  #
  # Frontend reverse proxy configuration
  #

  include profile::nginx

  case $ssl {
    true : {
      $ssl_cert = "/var/local/tls/${title}/fullchain.pem"
      $ssl_key = "/var/local/tls/${title}/privkey.pem"
    }
    default : {
      $ssl_cert = undef
      $ssl_key = undef
    }
  }

  $_proxy_set_header = [
    'Host $host',
    'X-Real-IP $remote_addr',
    'X-Forwarded-For $proxy_add_x_forwarded_for',
    'Proxy ""',
  ] + $proxy_set_header + ($ssl ? { false => [], default => ['X-Forwarded-Proto "https"']})

  nginx::resource::server { $title:
    proxy            => $proxy,
    proxy_set_header => $_proxy_set_header,
    ssl              => $ssl,
    ssl_port         => $ssl_port,
    ssl_cert         => $ssl_cert,
    ssl_key          => $ssl_key,
    ssl_redirect     => $ssl,
  }

  nginx::resource::location { "acme-webroot-${title}":
    server               => $title,
    location             => '^~ /.well-known/acme-challenge/',
    ssl                  => false,
    www_root             => $acme_webroot,
    try_files            => [ '$uri =404' ],
    index_files          => [],
    location_cfg_prepend => { 'default_type' => 'text/plain' },
    require              => File[$acme_webroot],
    notify               => Class['nginx::service'],
  }

  #
  # SSL/TLS
  #

  if $ssl {

    include profile::letsencrypt

    profile::letsencrypt::certonly { $title:
      domains      => [ $title ],
      plugin       => 'webroot',
      acme_webroot => $acme_webroot,
      require      => Nginx::Resource::Location["acme-webroot-${title}"],
    }

  }

}
