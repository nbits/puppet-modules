# A podman rootless user
define profile::podman::rootless (
  String $user = $name,
  String $group = $name,
  Integer $uid = 2000000,
  Integer $gid = 2000000,
  Integer $subuid_count = 65536,
  Stdlib::Absolutepath $home = "/var/lib/${name}",
  Optional[Hash] $mirror = {
    location => '127.0.0.1:5000',
    insecure => true,
  },
) {

  podman::subuid { $user:
    subuid  => $uid + 1,
    count   => $subuid_count,
    require => User[$user],
  }

  podman::subgid { $group:
    subgid  => $gid + 1,
    count   => $subuid_count,
    require => Group[$group],
  }

  group { $group:
    ensure => present,
    gid    => $gid,
  }

  File {
    owner  => $uid,
    group  => $gid,
  }

  file { $home:
    ensure => directory,
    mode   => '0755',
  }

  user { $user:
    ensure  => present,
    home    => $home,
    uid     => $uid,
    gid     => $gid,
    require => Group[$group],
  }

  podman::rootless { $user: }

  file { "${home}/.config/containers":
    ensure  => directory,
    mode    => '0755',
    require => File["${home}/.config"],
  }

  file { "${home}/.config/containers/registries.conf":
    ensure  => file,
    mode    => '0644',
    content => epp('profile/podman/registries.conf.epp', {
      mirror => $mirror,
    }),
    require => File["${home}/.config/containers"],
  }

}
