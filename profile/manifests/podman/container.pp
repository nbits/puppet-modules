# Define a container instance
define profile::podman::container (
  String $image,
  String $user,
  String $command = '',
  Hash $flags = {},
) {

  podman::container { $name:
    image   => $image,
    command => $command,
    user    => $user,
    flags   => $flags,
    require => User[$user],
  }

}
