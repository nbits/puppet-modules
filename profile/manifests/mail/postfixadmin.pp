# Setup Postfixadmin to manage virtual accounts
class profile::mail::postfixadmin (
  String $dbuser,
  String $dbpass,
  String $dbhost,
  String $dbname,
  String $auth_listen = '127.0.0.1:8080',
) {

  ### Package

  # XXX Remove once all Postfixadmin systems are running Bookworm
  if $::lsbdistcodename == 'bullseye' {
    apt::source { 'bookworm':
      location => 'http://deb.debian.org/debian',
      release  => 'bookworm',
    }

    apt::pin { 'bookworm':
      codename => 'bookworm',
      priority => 2,
    }

    apt::pin { 'postfixadmin':
      packages => [ 'postfixadmin' ],
      codename => 'bookworm',
      priority => 500,
    }
  }

  ensure_packages([ 'postfixadmin', 'postfix-mysql' ])

  ### Database config

  include profile::mysql::server

  profile::mysql::db { $dbname:
    user => $dbuser,
    pass => $dbpass,
    host => $dbhost,
  }

  file { '/etc/postfixadmin/dbconfig.inc.php':
    ensure  => file,
    content => epp('profile/postfixadmin/dbconfig.inc.php.epp', {
      dbuser   => $dbuser,
      dbpass   => $dbpass,
      dbname   => $dbname,
      dbserver => $dbhost,
    }),
    owner   => 'root',
    group   => 'www-data',
    mode    => '0640',
    require => Package['postfixadmin'],
  }

  ### vmail user

  user { 'vmail':
    home   => '/var/mail/vmail',
    system => true,
    shell  => '/usr/sbin/nologin',
  }

  file { '/var/mail/vmail':
    ensure  => 'directory',
    owner   => 'vmail',
    group   => 'vmail',
    mode    => '0750',
    require => User['vmail'],
  }

  ### SQL maps

  $sql_dir = '/etc/postfix/sql'

  file { $sql_dir:
    ensure  => 'directory',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Package['postfix'],
  }

  $sql_files = [
    'mysql_virtual_alias_maps.cf',
    'mysql_virtual_alias_domain_maps.cf',
    'mysql_virtual_alias_domain_catchall_maps.cf',
    'mysql_virtual_domains_maps.cf',
    'mysql_virtual_mailbox_maps.cf',
    'mysql_virtual_sender_restrictions.cf',
    'mysql_virtual_alias_domain_mailbox_maps.cf',
    'mysql_relay_domains.cf',
    'mysql_transport_maps.cf',
    'mysql_virtual_mailbox_limit_maps.cf',
  ]

  $sql_files.each | String $file_name | {
    postfix::map { "${sql_dir}/${file_name}":
      path    => "${sql_dir}/${file_name}",
      content => epp("profile/postfixadmin/${file_name}.epp", {
        user     => $dbuser,
        password => $dbpass,
        hosts    => $dbhost,
        dbname   => $dbname,
      }),
      require => File[$sql_dir],
    }
  }

  ### PHP

  include php
  include profile::nginx

  $php_socket = '/var/run/php-fpm/postfixadmin'

  php::fpm::pool { 'postfixadmin':
    listen       => $php_socket,
    listen_owner => www-data,
    listen_group => www-data,
  }

  nginx::resource::server { '127.0.0.1':
    www_root             => '/usr/share/postfixadmin/public',
    auth_basic           => 'postfixadmin',
    auth_basic_user_file => '/var/local/htpasswd/postfixadmin',
    index_files          => [ 'index.php' ],
  }

  nginx::resource::location { '~ \.php$':
    server   => '127.0.0.1',
    www_root => '/usr/share/postfixadmin/public',
    fastcgi  => 'unix:/var/run/php-fpm/postfixadmin',
  }

  ### Python virtualenv for Postfixadmin authenticator

  file { '/usr/local/lib/postfixadmin-auth':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/usr/local/lib/postfixadmin-auth/requirements.txt':
    ensure  => 'file',
    content => 'dovecotauth==1.0.1',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => File['/usr/local/lib/postfixadmin-auth'],
  }

  class { 'python':
    version    => 'python3',
    virtualenv => 'present',
  }

  python::virtualenv { '/usr/local/lib/postfixadmin-auth/venv':
    requirements => '/usr/local/lib/postfixadmin-auth/requirements.txt',
    systempkgs   => true,
    version      => '3',
    require      => [
      File['/usr/local/lib/postfixadmin-auth/requirements.txt'],
      Class['python'],
    ],
  }

  ### Nginx mail reverse-proxy authenticator

  file { '/usr/local/lib/python3.11/dist-packages/postfixadmin_auth.py':
    ensure => 'file',
    source => 'puppet:///modules/profile/postfixadmin/postfixadmin_auth.py',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Service['postfixadmin-auth'],
  }

  ensure_packages([ 'python3-mysqldb', 'python3-waitress' ])

  systemd::unit_file { 'postfixadmin-auth.service':
    content => epp('profile/postfixadmin/postfixadmin-auth.service.epp', { listen => $auth_listen }),
    notify  => Service['postfixadmin-auth'],
  }

  service { 'postfixadmin-auth':
    ensure  => 'running',
    require => [
      File['/usr/local/lib/python3.11/dist-packages/postfixadmin_auth.py'],
      Systemd::Unit_file['postfixadmin-auth.service'],
      Package['python3-waitress'],
      Package['python3-mysqldb'],
      Python::Virtualenv['/usr/local/lib/postfixadmin-auth/venv'],
    ],
  }

}
