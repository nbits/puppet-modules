# Expose the Puppet Server as an onion service (for node bootstrapping purposes)
class profile::puppet::server::onion_service (
  String $server_ip = '127.0.0.1',
  Integer $server_port = 8140,
) {

  include tor

  tor::daemon::onion_service { 'puppet':
    ports => [ "${server_port} ${server_ip}" ],
  }

}
