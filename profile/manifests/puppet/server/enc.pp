# External Node Classifier
class profile::puppet::server::enc {

  file { '/usr/local/bin/puppet_node_classifier':
    ensure => file,
    source => 'puppet:///modules/profile/puppet/puppet_node_classifier',
    owner  => root,
    group  => root,
    mode   => '0755';
  }

}
