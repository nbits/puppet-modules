# Manage Nginx web server
class profile::nginx (
  Hash[String[1], String[1]] $extra_mime_types = {},
) {

  class { 'nginx':
    manage_repo                  => false,
    mime_types_preserve_defaults => true,
    mime_types                   => $extra_mime_types,
  }

  exec { 'profile::nginx systemctl daemon-reload':
    command     => '/bin/systemctl daemon-reload',
    refreshonly => true,
  }

  file { '/etc/systemd/system/nginx.service.d':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
  }

  # The nginx Puppet module uses /run/nginx by default as a runtime directory,
  # but that directory is not created by the Systemd Unit file included in the
  # Debian package. See: https://github.com/voxpupuli/puppet-nginx/issues/1372
  file { '/etc/systemd/system/nginx.service.d/override.conf':
    ensure  => file,
    owner   => 0,
    group   => 0,
    mode    => '0644',
    content => "[Service]\nRuntimeDirectory=nginx\n",
    require => File['/etc/systemd/system/nginx.service.d'],
    notify  => [
      Exec['profile::nginx systemctl daemon-reload'],
      Service['nginx'],
    ]
  }

}
