# Allow inbound Mumble traffic
class profile::mumble::firewall () {

  ['tcp', 'udp'].each | String $proto | {
    firewall { "100 accept inbound ${proto.upcase()} on Mumble port":
      dport  => [ 64738 ],
      proto  => $proto,
      action => 'accept',
    }
  }

}
