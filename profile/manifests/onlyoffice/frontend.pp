# A frontend for ONLYOFFICE
define profile::onlyoffice::frontend (
  Integer $public_port = 443,
  String $local_address = '127.0.0.1',
  Integer $local_port = 8000,
  Boolean $ssl = true,
) {

  include profile::nginx
  include profile::letsencrypt

  nginx::resource::map { 'proxy_connection':
    default  => 'upgrade',
    string   => '$http_upgrade',
    mappings => { '""' => 'close' },
  }

  profile::reverse_proxy { $name:
    ssl              => $ssl,
    ssl_port         => $public_port,
    proxy            => "http://${local_address}:${local_port}",
    proxy_set_header => [
      'Upgrade $http_upgrade',
      'Connection $proxy_connection',
    ],
  }

  nginx::resource::location { '/info':
    server        => $name,
    location_deny => [ 'all' ],
    index_files   => [],
    ssl_only      => $ssl,
  }

  firewall { "100 allow TCP access to ONLYOFFICE instance ${name}":
    dport  => [ $public_port ],
    proto  => 'tcp',
    action => 'accept',
  }

  @@profile::monitoring::services::http { "profile::onlyoffice::frontend: ${name}":
    http_vhost => $name,
    https_port => $public_port,
    http_uri   => '/healthcheck',
    config     => {
      host_name => $trusted['certname'],
      vars      => { http_string => '-:"true"' },
    },
    tag        => "environment:${::environment}",
  }

}
