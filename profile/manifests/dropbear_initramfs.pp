# Manage dropbear-initramfs:
#
#   - Install the package.
#
#   - Populate the /etc/dropbear-initramfs/authorized_keys file with
#      keys from # `profle::account::users`.
#
#   - Update (all) initramfs when there are changes to the above file.
#
class profile::dropbear_initramfs (
  Hash $users = lookup('profile::accounts::users', Hash, 'first', {})
) {

  ensure_packages([ 'dropbear-initramfs' ])

  exec { 'dropbear-initramfs update-initramfs':
    command     => '/usr/sbin/update-initramfs -k all -u',
    user        => root,
    refreshonly => true,
  }

  $authorized_keys_file = $facts['os']['distro']['codename'] ? {
    'bullseye' => '/etc/dropbear-initramfs/authorized_keys',
    default => '/etc/dropbear/initramfs/authorized_keys',  # bookworm and newer, hopefully
  }

  concat { $authorized_keys_file:
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package['dropbear-initramfs'],
    notify  => Exec['dropbear-initramfs update-initramfs'],
  }

  $users.each | String $domain_regexp, Hash $domain_users | {
    if $trusted['certname'] =~ Regexp($domain_regexp) {
      $domain_users.each | String $username, Hash $params | {
        if $params['sshkeys'] {
          $params['sshkeys'].each | String $key | {
            concat::fragment { "dropbear-initramfs ${username} ${key}":
              target  => $authorized_keys_file,
              content => "${key}\n",
            }
          }
        }
      }
    }
  }

}
