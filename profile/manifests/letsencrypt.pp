# Manage Let's Encrypt certificates
class profile::letsencrypt (
  Optional[String] $email            = undef,
  Stdlib::Absolutepath $acme_webroot = '/var/www/acme',
  Boolean $unsafe_registration       = true,
  Stdlib::Absolutepath $tls_root     = '/var/local/tls',
  Hash $config = { 'server' => 'https://acme-staging-v02.api.letsencrypt.org/directory' },
  Hash $certificates = {},
) {

  class { 'letsencrypt':
    email               => $email,
    unsafe_registration => $unsafe_registration,
    config              => $config,
  }

  # XXX: Find a better way to ensure directories recursively
  exec { "Create ${acme_webroot}":
    command => "/bin/mkdir -p ${acme_webroot}",
    unless  => "/usr/bin/test -d ${acme_webroot}",
    user    => 'root',
  }

  file { $acme_webroot:
    ensure  => directory,
    owner   => www-data,
    group   => www-data,
    mode    => '0755',
    require => Exec["Create ${acme_webroot}"]
  }

  file { $tls_root:
    ensure => directory,
    owner  => www-data,
    group  => www-data,
    mode   => '0770',
  }

  file { '/usr/local/sbin/create-tls-links.sh':
    ensure => file,
    source => 'puppet:///modules/profile/letsencrypt/create-tls-links.sh',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  # The following package provides a snakeoil TLS certificate that is used in
  # the script above.
  ensure_packages([ 'ssl-cert' ])

  ### Configure TLS certificates from class parameter
  include profile::nginx

  nginx::resource::server { 'acme-default-webroot':
    listen_port          => 80,
    listen_options       => 'default_server',
    ssl                  => false,
    use_default_location => false,
    index_files          => [],
  }

  nginx::resource::location { 'acme-default-webroot':
    server               => 'acme-default-webroot',
    location             => '^~ /.well-known/acme-challenge/',
    ssl                  => false,
    www_root             => $acme_webroot,
    try_files            => [ '$uri =404' ],
    index_files          => [],
    location_cfg_prepend => { 'default_type' => 'text/plain' },
    require              => File[$acme_webroot],
  }

  nginx::resource::location { 'acme-fallback':
    server              => 'acme-default-webroot',
    location            => '/',
    ssl                 => false,
    index_files         => [],
    location_cfg_append => { 'return' => '403' },
  }

  firewall { '100 allow TCP access to port 80 for Let\'s Encrypt':
    dport  => [ 80 ],
    proto  => 'tcp',
    action => 'accept',
  }

  $certificates.each | String $name, Hash $config | {
    $_config = deep_merge($config, { require => Firewall['100 allow TCP access to port 80 for Let\'s Encrypt'] })
    profile::letsencrypt::certonly { $name:
      * => $_config,
    }
  }

  Profile::Letsencrypt::Certonly <<| tag == $::domain |>>

}
