# Configure ONLYOFFICE using Hiera:
#
#     role::onlyoffice::instances:
#       instance_name:
#          uid: 5000000
#          gid: 5000000
#          jwt_secret: very-secretive-passphrase
#          local_port: 8000
#          public_port: 443
#          public_address: onlyoffice.example.org
#
# View application logs:
#
#   instance="myinstance"
#   cd /var/lib/onlyoffice/${instance}
#   sudo -u onlyoffice_${instance} podman logs --tail=1 -f onlyoffice_${instance}_onlyoffice
class profile::onlyoffice (
  Hash $instances = {},
) {

  include profile::podman

  file { '/var/lib/onlyoffice':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  $instances.each | String $name, Hash $config | {
    $_config = deep_merge($config, { require => File['/var/lib/onlyoffice'] })
    profile::onlyoffice::instance { $name:
      * => $_config,
    }
  }

}
