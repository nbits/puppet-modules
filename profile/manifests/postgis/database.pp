# A PostGIS database resource definition
define profile::postgis::database (
  Hash $config = {},
  Stdlib::IP::Address::V4::Nosubnet $listen_address = '127.0.0.1',
) {

    postgresql::server::db { $title:
      * => $config,
    }

    postgresql::server::pg_hba_rule { "Allow connection to database ${title}":
      description => "Allow connection to database ${title}",
      type        => 'hostssl',
      database    => $title,
      user        => $config['user'],
      address     => 'all',
      auth_method => 'md5',
    }

    -> postgresql_conn_validator { "Validate connection to database ${title}":
      host        => $listen_address,
      db_username => $config['user'],
      db_password => $config['password'],
      db_name     => $title,
      psql_path   => '/usr/bin/psql',  # not automatically resolved, for some reason
    }

    # Turn on PostGIS extensions
    $extensions = [
      'plpgsql',
      'postgis',
      'postgis_raster',
      'postgis_topology',
    ]

    $extensions.each | String $ext | {
      exec { "Create ${ext} PostGIS extension in database ${title}":
        command => "/usr/bin/psql ${title} -c \"CREATE EXTENSION ${ext};\"",
        unless  => "/usr/bin/psql ${title} --csv -c \"SELECT count(*) FROM pg_extension WHERE extname = '${ext}';\" | /usr/bin/grep 1",
        user    => 'postgres',
        require => Postgresql::Server::Db[$title],
      }
    }
}
