# Libvirt profile
#
# You can define VMs and networks like this:
#
#   profile::libvirt::vms:
#     vm-name:
#       cpus: 1
#       memory: 1024
#       disk: 10G
#       network: my-network
#
#   profile::libvirt::networks:
#     network-name:
#       bridge: bridge-name
#       forward_mode: open
#       ip_address: 192.168.122.1
#       ip_netmask: 255.255.255.0
#       dhcp_start: 192.168.122.2
#       dhcp_end: 192.168.122.254
#       subnet: 192.168.122.0/24
#
# A default network will always be defined because we need it to generate the
# template VM image.
#
# Note: we prefer to use "forward_mode=open" because then libvirt will not add
# its own firewall rules and we can have finer control of our network.
#
class profile::libvirt (
  Hash   $vms          = {},
  Hash   $networks     = {},
  String $volume_group = 'default',
) {

  include libvirt

  # virt-clone in Buster is buggy, let's install it from backports instead
  include profile::apt
  apt::pin { 'virtinst from backports':
    packages => [ 'virtinst' ],
    release  => 'buster-backports',
    priority => 500,
  }

  ensure_packages([ 'bridge-utils', 'virtinst', 'qemu-utils', 'libguestfs-tools' ])

  file { '/usr/local/sbin/clone-vm':
    source => 'puppet:///modules/profile/libvirt/clone-vm',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/sbin/delete-vm':
    source => 'puppet:///modules/profile/libvirt/delete-vm',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  class { 'profile::libvirt::template':
    require => Package['virtinst'],
  }

  # make sure the 'default' network is defined

  $default_network = {
    bridge       => 'virbr0',
    forward_mode => 'open',
    ip_address   => '192.168.122.1',
    ip_netmask   => '255.255.255.0',
    dhcp_start   => '192.168.122.2',
    dhcp_end     => '192.168.122.254',
    subnet       => '192.168.122.0/24',
    autostart    => true,
  }

  $default_network_overlay = $networks['default'] ? {
    undef   => {},
    default => $networks['default'],
  }

  $networks_real = $networks + { 'default' => $default_network + $default_network_overlay }

  $networks_real.each | String $network, Hash $config | {
    profile::libvirt::network { $network:
      bridge       => $config['bridge'],
      forward_mode => $config['forward_mode'],
      ip_address   => $config['ip_address'],
      ip_netmask   => $config['ip_netmask'],
      dhcp_start   => $config['dhcp_start'],
      dhcp_end     => $config['dhcp_end'],
      subnet       => $config['subnet'],
      autostart    => $config['autostart'],
    }
  }

  $vms.each | String $vmname, Hash $config | {
    $network = $config['network'] ? { undef => 'default', default => $config['network'] }
    $puppet_server = $config['puppet_server'] ? { undef => 'puppet', default => $config['puppet_server'] }
    profile::libvirt::vm { $vmname:
      cpus          => $config['cpus'],
      memory        => $config['memory'],
      disk          => $config['disk'],
      volume_group  => $volume_group,
      template      => 'debian',
      network       => $network,
      puppet_server => $puppet_server,
      require       => Profile::Libvirt::Network[$config['network']],
    }
  }

}
