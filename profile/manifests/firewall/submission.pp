# Firewall for submission port
class profile::firewall::submission () {

  firewall { '100 allow TCP access on submission port':
    dport  => [ 587 ],
    proto  => 'tcp',
    action => 'accept',
  }

}
