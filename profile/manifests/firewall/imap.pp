# Firewall for IMAP
class profile::firewall::imap () {

  firewall { '100 allow TCP access on IMAP port':
    dport  => [ 143 ],
    proto  => 'tcp',
    action => 'accept',
  }

}
