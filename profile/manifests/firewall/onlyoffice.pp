# Firewall rules for ONLYOFFICE DocumentServer
class profile::firewall::onlyoffice () {

  firewall { '100 allow TCP access on ONLYOFFICE port':
    dport  => [ 4430 ],
    proto  => 'tcp',
    action => 'accept',
  }

}
