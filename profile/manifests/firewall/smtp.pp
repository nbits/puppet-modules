# A SMTP server's firewall
class profile::firewall::smtp () {

  firewall { '100 allow TCP access on SMTP port':
    dport  => [ 25 ],
    proto  => 'tcp',
    action => 'accept',
  }

}
