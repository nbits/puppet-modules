# A web server's firewall
class profile::firewall::web {

  firewall { '100 allow HTTP/S access':
    dport  => [ 80, 443 ],
    proto  => 'tcp',
    action => 'accept',
  }

}
