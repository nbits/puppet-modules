# A server's firewall
class profile::firewall::server (
  String $public_ip                   = $facts['networking']['ip'],
  String $public_iface                = 'eth0',
  Optional[String] $additional_ip     = undef,
  Optional[String] $additional_bridge = undef,
  Optional[Stdlib::Ip::Address] $web_forward = undef,
) {

  if $web_forward {
    firewall { '100 forward HTTP/S ports to web VM':
      table       => 'nat',
      chain       => 'PREROUTING',
      iniface     => $public_iface,
      proto       => 'tcp',
      destination => $public_ip,
      dport       => [ 80,443 ],
      jump        => 'DNAT',
      todest      => $web_forward,
    }

    firewall { '100 accept TCP connections to the webserver':
      chain       => 'FORWARD',
      proto       => 'tcp',
      destination => $web_forward,
      dport       => [ 80,443 ],
      action      => 'accept',
    }
  }

  if ($additional_ip and $additional_bridge) {
    firewall { '105 forward from additional IP':
      chain   => 'FORWARD',
      source  => $additional_ip,
      iniface => $additional_bridge,
      proto   => 'all',
      action  => 'accept',
    }

    firewall { '106 forward to additional IP':
      chain       => 'FORWARD',
      destination => $additional_ip,
      outiface    => $additional_bridge,
      proto       => 'all',
      action      => 'accept',
    }
  }
}
