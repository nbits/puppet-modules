# Manage network interfaces and routes
class profile::network (
  Hash $interfaces = {},
) {

  create_resources('network::interface', $interfaces)

  if $::hostname != 'puppet' {
    host { $trusted['certname']:
      ip           => '127.0.1.1',
      host_aliases => $::hostname,
    }
  }

}
