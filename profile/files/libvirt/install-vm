#!/bin/sh
#
# Install a VM using virt-install
#
# Note: Brings up and tears down the network. This is currently fine because
#       this script is only used by the template VM installer.

set -ex

if [ "${#}" -ne 5 ]; then
        echo "Usage: ${0} NAME CPUS MEMORY NETWORK DISK"
        exit 1
fi

NAME=${1}
CPUS=${2}
MEMORY=${3}
NETWORK=${4}
DISK=${5}
PRESEED_FILE="/var/lib/libvirt/preseed.cfg"

main() {
	virsh net-list | tail -n +3 | cut -d ' ' -f 2 | grep -q ^${NETWORK}$ \
		|| virsh net-start ${NETWORK}
        virt-install \
                --connect qemu:///system \
                --virt-type kvm \
                --name ${NAME} \
                --memory ${MEMORY} \
                --disk ${DISK} \
                --network network=${NETWORK} \
                --virt-type kvm \
                --os-variant debian10 \
                --location http://ftp.us.debian.org/debian/dists/stable/main/installer-amd64/ \
                --vcpus ${CPUS} \
                --os-type linux \
                --console pty,target_type=serial \
                --graphics none \
                --initrd-inject ${PRESEED_FILE}  \
                --extra-args 'console=ttyS0,115200n8 serial' \
		--noautoconsole \
		--events on_reboot=destroy
	while virsh list | grep ${NAME}; do sleep 1; done
	virsh net-destroy ${NETWORK}
}

main
