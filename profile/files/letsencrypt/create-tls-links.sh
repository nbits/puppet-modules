#!/bin/sh

if [ ${#} -lt 2 -o ${#} -gt 3 ]; then
	echo "Usage: ${0} DOMAIN TLS_ROOT [RELOAD]"
	exit 1
fi

DOMAIN=${1}
TLS_ROOT=${2}
RELOAD=${3}
PREFIX=${TLS_ROOT}/${DOMAIN}
LETSENCRYPT_ROOT=/etc/letsencrypt/live/${DOMAIN}

if [ ! -d ${TLS_ROOT} ]; then
	echo "Error: TLS root does not exist (${TLS_ROOT})"
	exit 1
fi

create_links() {
	mkdir -p ${PREFIX}
	chown www-data:www-data ${PREFIX}
	for i in fullchain privkey; do
		ln -sf ${LETSENCRYPT_ROOT}/${i}.pem ${PREFIX}/${i}.pem
	done
}

fallback_to_snakeoil() {
	ln -sf /etc/ssl/certs/ssl-cert-snakeoil.pem ${PREFIX}/fullchain.pem
	ln -sf /etc/ssl/private/ssl-cert-snakeoil.key ${PREFIX}/privkey.pem
}

main() {
	create_links
	test -f "$( readlink -f ${PREFIX}/fullchain.pem )" || fallback_to_snakeoil
	if [ -n "${RELOAD}" ]; then
		systemctl reload nginx
	fi
}

main
