Facter.add(:backup_sshkey) do
  setcode do
    begin
      File.read('/root/.ssh/id_ed25519_backups.pub').split(' ')
    rescue Errno::ENOENT
      ''
    end
  end
end
